﻿using UnityEngine;

namespace UnityTechnologies.Scripts
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField, Min(0)] private float _speed;
        [SerializeField] private Transform _camera;
        [Header("Steps Simulation")]
        [SerializeField] private float _shift = 0.1f;
        [SerializeField] private float _step = 1;
        [SerializeField] private AnimationCurve _curve;
        
        private CharacterController _characterController;
        private bool _moving;
        private Vector3 _direction;
        private float _stepTimer;
        private float _offset;
        private float _startY;

        private void Awake()
        {
            _startY = _camera.localPosition.y;
            _characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
#if UNITY_EDITOR
            ControlPCInput();
#else
            ControlMobileInput();
#endif

            if (_moving)
                Move();
        }

        private void ControlPCInput()
        {
            _moving = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D);
            
            if (Input.GetKeyDown(KeyCode.A))
                _direction += Vector3.left;
            
            if (Input.GetKeyUp(KeyCode.A))
                _direction -= Vector3.left;
            
            if (Input.GetKeyDown(KeyCode.D))
                _direction += Vector3.right;
            
            if (Input.GetKeyUp(KeyCode.D))
                _direction -= Vector3.right;
        }

        private void ControlMobileInput()
        {
            if(Input.touchCount <= 0)
                return;

            Touch touch = Input.GetTouch(0);
            
            if (touch.phase is TouchPhase.Moved or TouchPhase.Stationary or TouchPhase.Began)
            {
                _moving = true;
                _direction = touch.position.x < (Screen.width / 2f) ? Vector3.left : Vector3.right;
            }
            else if(touch.phase == TouchPhase.Ended)
            {
                _moving = false;
            }
        }

        private void Move()
        {
            _characterController.Move(_direction * (Time.deltaTime * _speed));
            DoCameraBounce();
        }

        private void DoCameraBounce()
        {
            _stepTimer += Time.deltaTime * _speed;
            float t = Mathf.Clamp01(_stepTimer / _step);

            if (_stepTimer >= _step)
                _stepTimer = 0;

            float value = _curve.Evaluate(t);
            _offset = _shift * value;

            Vector3 pos = _camera.localPosition;
            pos.y = _startY + _offset;
            _camera.localPosition = pos;
        }
    }
}