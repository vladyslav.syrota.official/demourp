﻿using System;
using TMPro;
using UnityEngine;

namespace UnityTechnologies.Scripts
{
    public class FPSCounter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField, Min(0.1f)] private float _updateRate = 1;

        private float _fpsSum;
        private int _frames;
        private float _timer;

        private void Awake()
        {
#if !UNITY_EDITOR
            Application.targetFrameRate = 120;      
#endif
        }

        private void Update()
        {
            _fpsSum += 1 / Time.deltaTime;
            _frames++;
            _timer += Time.deltaTime;

            if (_timer >= _updateRate)
            {
                _timer = 0;
                _text.text = $"FPS: {_fpsSum / _frames:F0}";
                _fpsSum = 0;
                _frames = 0;
            }
        }
    }
}