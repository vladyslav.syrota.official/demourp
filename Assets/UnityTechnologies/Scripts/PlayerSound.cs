﻿using UnityEngine;

namespace UnityTechnologies.Scripts
{
    public class PlayerSound : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _collisionSound;
        
        private Vector3 _lastHitPosition;

        private void LateUpdate()
        {
            if (Vector3.Distance(_lastHitPosition, transform.position) > 0.1f)
                _lastHitPosition = Vector3.zero;
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            if(Vector3.Distance(_lastHitPosition, transform.position) <= 0.1f)
                return;
            
            _lastHitPosition = transform.position;
            _audioSource.clip = _collisionSound;
            _audioSource.Play();
        }
    }
}